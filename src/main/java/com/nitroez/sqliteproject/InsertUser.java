/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author ckitt
 */
public class InsertUser {
    public static void main(String[] args) {
      Connection conn = null;
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         conn = DriverManager.getConnection("jdbc:sqlite:user.db");
         conn.setAutoCommit(false);
         System.out.println("Opened database successfully");

         stmt = conn.createStatement();
         String sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (1,'NitroEz','1234');"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (2,'Sedz','1234');";
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (3,'Pzx','1234');";
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (4,'Zadq','1234');";
         stmt.executeUpdate(sql);

         stmt.close();
         conn.commit();
         conn.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Records created successfully");
    }
}
