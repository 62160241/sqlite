/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nitroez.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ckitt
 */
public class ConnectDatabase {
    public static void main(String[] args) {
        Connection conn=null;
        String dbName="user.db";
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:"+dbName);
        } catch (ClassNotFoundException ex) {
            System.out.println("Library not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to open database");
        }
        System.out.println("Opened Database successfully");
    }
}
